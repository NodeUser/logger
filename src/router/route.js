
const express = require('express');
var cors = require('cors');
const logger = require('../util/MiddleWare');
var bodyParser = require('body-parser');
//import Home from '../controllers/HomeService';
// import Item from '../controllers/ItemService';
var databaseLogEntry = require('../util/LogDbEntry');
const log = require('../controllers/Logs')
const itemController = require('../controllers/itemController');

/** Middleware Used */
const router = express.Router();
router.use(cors())
router.use(bodyParser.urlencoded({ extended: true }))
router.use(bodyParser.json())
router.use(log.logRequestWinston);
router.use( databaseLogEntry.loggerDataBaseAutomatic)
/** Middleware Used */

/** Api Routes.... */
router.get('/', (req, res) => { res.send('hi') });
//router.post('/add', databaseLogEntry.loggerDataBase, itemController.additem);
router.post('/add',itemController.additem);
router.get('*', function (req, res) {
    // res.sendFile(__dirname+’/public/error.html’);
    logger.error('Not Found Route', req.msg);
    res.send('Not Found Route', req.msg);
});
router.post('*', function (req, res) {
    // res.sendFile(__dirname+’/public/error.html’);
    logger.error('Not Found Route', req.msg);
    res.send('Not Found Route');
});
/** Api Routes.... */
module.exports = router;