const databaseConn = require('../database/dataBaseConnection');
module.exports = {
    loggerDataBase:(req , res , next ) => {   
    
       databaseConn.loggerDataBase(req);
     
          next();
},
loggerDataBaseAutomatic:(req , res , next ) => {     
   databaseConn.autoTablecreation(req);
 
      next();
},
}
