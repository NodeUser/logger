
var moment = require('moment-timezone');
const { createLogger, format, transports } = require('winston');
var winston = require('winston');
const { combine, label, timestamp, printf } = format;
const myFormat = printf((info) => `${info.timestamp} [${info.level}]: - ${info.message}`);
var logger = createLogger({
  transports: [
    new winston.transports.File({
      level: 'debug',
      filename: './logs/all-logs.log',
      format: combine(
        label({ label: 'main' }),
        timestamp(),
        myFormat
      ),
      /**To add the logging  info a particular format */
      handleExceptions: true,
      json: true,
      maxsize: 5242880, //5MB
      maxFiles: 5000,
      colorize: false
    }),
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true
    })
  ],
  exitOnError: false
})
console.log("logger called........");
//logger.add(new winston.transports.Console(options));
module.exports = logger;
