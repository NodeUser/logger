﻿//import { AppConfig } from './../../config/AppConfig';
export class ProjectConstants {
    public static readonly SUCCESS = true;
    public static readonly LOCALPORT = 8000;
    public static readonly QAPORT = process.env.PORT || 8000;
    public static readonly STAGINGPORT = process.env.PORT || 8000;
    public static readonly PRODPORT = process.env.PORT || 8000;
   // postgres://postgres:ztech@44@localhost:5432/log_Records
    public static readonly LOCALDATABASENAME = "log_Records";
    public static readonly QADATABASENAME = "log_Records";
    public static readonly STAGINGDATABASENAME = "log_Records";
    public static readonly PRODDATABASENAME = "log_Records";

    public static readonly LOCALDATABASEPORT = 5432;
  
    public static readonly QADATABASEPORT = 5432;
    public static readonly STAGINGDATABASEPORT = 5432;
    public static readonly PRODDATABASEPORT = 5432;
}
export class CommonConstants {

    public static readonly DAYS_ = 1
}


export class ApiNames {

    public static readonly API = "/api/";
    public static readonly HOME = `${ApiNames.API}`;
    public static readonly ADD_ITEM = `${ApiNames.API}addItem`;
    public static readonly ADD_ITEM_1 = `${ApiNames.API}addItem1`;
    
}
   
export class ErrorCodes {
    public static readonly NO_SERVICE_PROVIDER = 101;
    public static readonly OK = 200;
    public static readonly NO_CONTENT = 204;
    public static readonly BAD_REQUEST = 400;
    public static readonly UNAUTHORIZED = 401;
    public static readonly INVALID_AUTHTOKEN = 403;
    public static readonly PAGE_NOT_FOUND = 404;
    public static readonly NOT_ACCEPTABLE = 406;
    public static readonly REQUEST_TIMEOUT = 408;
    public static readonly CONFLICT = 409;
    public static readonly NO_RESPONSE = 444;
    public static readonly INVALID_TOKEN = 498;
    public static readonly TOKEN_REQUIRED = 499;
}
export class ResponseMessages {

    public static readonly error = (code: Number, msg: string) => {
        return {
            errorCode: code,
            errorMessage: msg
        }
    }
    public static readonly ERROR_ = ResponseMessages.error(ErrorCodes.CONFLICT, "error while insertion");
    public static readonly ERROR_DATABSE_CONNECTION = ResponseMessages.error(ErrorCodes.CONFLICT, "error while database connection");
}
