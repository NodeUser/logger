
const logger = require('../util/MiddleWare');

module.exports ={
        logRequestWinston: (req, res, next) => {
            let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
            if (req.params) {
                fullUrl += [].join.call(req.params, "/");
            }

            let msg = "Req" + ": " + fullUrl;
            msg += "\r\n Body: " + JSON.stringify(req.body)
            req.msg = msg;
            logger.info(msg);
            next();
        }
    }
