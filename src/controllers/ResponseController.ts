/** import { Request, Response } from 'express-serve-static-core';

import { ResponseMessages } from './../util/Contants';
import { logger } from '../util/MiddleWare';
const log = require('../controllers/LogController');
const databaseConn = require('../database/dataBaseConnection');

interface IRESPONSE {
    isSuccess: any;
    data: any;
    errorCode?: any,
    errorMessage?: any,
    cause?: any


}

export default class ResponseController {

    private static ERROR = 'false';
    private static SUCCESS = 'true';

    private static response(req: Request, data: any): IRESPONSE {

        const successRes: IRESPONSE = {
            isSuccess: ResponseController.SUCCESS,
            data: data,
            errorCode: 0,
            errorMessage: "",
            cause: ""

        };
       
        /** Winston logger */
//         log.logResponseWinston(req, successRes);
//         return successRes;
//     };

//     private static error(req: Request, data: any, errorCode: any, errorMessage: any, cause: string): IRESPONSE {

//         // if (cause) {
//         //     data.cause = cause;
//         // }

//         const errorRes: IRESPONSE =
//         {
//             isSuccess: ResponseController.ERROR,
//             data: data,
//             errorCode: errorCode,
//             errorMessage: errorMessage,
//             cause: cause
//         };
//         return errorRes;
//     }

//     static sendResponse(req: Request, res: Response, data: any, statusCode: any): void {
//         const response = ResponseController.response(req, data);
//         if (statusCode >= 100 && statusCode < 600) {
//             //  res.statusMessage = message; 
//             res.status(statusCode).send(response);
//         }
//         else {
//             res.status(500).send(response);
//         }
//     }

//     static sendError(req: Request, res: Response, data: any, errorCode: any, errorMessage: any, cause: string, statusCode: any): void 
//     {
//         const error = ResponseController.error(req, data, errorCode, errorMessage, cause);
//         let msg = "Error" + ": " + JSON.stringify(error);

//          logger.error(msg);
//         databaseConn.loggexception(req , JSON.stringify(error));
//         res.send(error);
    
//     }

//     public static catchError(req: Request, res: Response, err: any) {
//         let errorObjJSON: any = JSON.parse(JSON.stringify(err));
 

//         if (errorObjJSON.errorCode) {
//        //     console.log(errorObjJSON.errorCode);
//             /** Contains all the errors that are thrown away.... */
   
//             err.code = errorObjJSON.errorCode;
//             err.errorMessage = errorObjJSON.errorMessage;
       
//             ResponseController.sendError(req, res, {}, err.code, err.errorMessage, "", err.code);

//         }
//         else {
      
//             if (err.code) {
//                 ResponseController.sendError(req, res, {}, err.code, err.message, `ErrCode:${err.code}, ErrMsg:${err.message}`, err.code); 
//             }
//             ResponseController.sendError(req, res, {}, err.code, err.message, `ErrCode:${err.code}, ErrMsg:${err.message}`, err.code);
//         }
//     }

// }

