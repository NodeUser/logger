const databaseConn = require('../database/dataBaseConnection');
const logger = require('../util/MiddleWare');
module.exports = {
   
    customResponse: (req, res, status_code, message, data) => {
        var result = {
            status: {
                code: status_code,
                message:message
            }
        };
        if (typeof data !== 'undefined') {
            result.data = data;
        } else {
            result.data = {};
        }        
        return res.json(result);
    },

    customerrResponse: (req, res, status_code, message, err) => {
        var result = {
            status: {
                code: status_code,
                message:message
            },
            err:err
        };
     
        let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        if (req.params) {
            fullUrl += [].join.call(req.params, "/");
        }

        let msg = "Req" + ": " + fullUrl;
        msg += "\r\n Body: " + JSON.stringify(req.body)
        // req.msg = msg;
        let errmsg = "\r\n exception : " + JSON.stringify(err)
        logger.error(errmsg); 
        databaseConn.loggexception(req , errmsg , msg );

        return res.json(result);
    }
    }    
