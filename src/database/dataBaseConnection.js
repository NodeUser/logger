const pg = require('pg');
const logger = require('../util/MiddleWare');
module.exports = {
    dbconn: () => {
        return new Promise(function (resolve, reject) {
            const connectionString = process.env.DATABASE_URL || 'postgres://postgres:ztech@44@localhost:5432/log_Records';
            let client = new pg.Client(connectionString);
            client.connect(function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(client);
                }
            });
        });
    },
    loggerDataBase: (req) => {
        return new Promise(function (resolve, reject) {
            const connectionString = process.env.DATABASE_URL || 'postgres://postgres:ztech@44@localhost:5432/log_Records';
            let client = new pg.Client(connectionString);
            client.connect(function (err) {
                if (err) {
                    // reject(err);
                    log.error('Error While Logging ');
                }
                else {
                    client.query('INSERT INTO logger(message , level) values($1, $2) RETURNING id;',
                        [req.msg, 'info'],
                        function (err, result) {
                            if (err) {
                                //  reject(err);
                            }
                            else {

                                req.loggerId = result.rows[0].id;
                                console.log(result.rows[0].id, result.rows[0]);
                                client.end();
                            }

                        });
                }
            });
        });
    },
    loggexception: (req, exception, requesturl, errorCode, errorMessage) => {
        return new Promise(function (resolve, reject) {
            const connectionString = process.env.DATABASE_URL || 'postgres://postgres:ztech@44@localhost:5432/log_Records';
            let client = new pg.Client(connectionString);
            client.connect(function (err) {
                if (err) {
                    // reject(err);
                    log.error('Error While Logging ');
                }
                else {
                    client.query('INSERT INTO loggerrec(message , level , requesturl , createddate ) values($1, $2 , $3 , $4 )',
                        [exception, 'error', requesturl, Date.now()],
                        function (err, result) {
                            if (err) {
                                //  reject(err);
                            }
                            else {

                                client.end();
                                // resolve(result);
                            }

                        });
                }
            });
        });
    },
    autoTablecreation: (req) => {
        //return new Promise(function (resolve, reject) {
        const connectionString = process.env.DATABASE_URL || 'postgres://postgres:ztech@44@localhost:5432/log_Records';
        let client = new pg.Client(connectionString);
        client.connect(function (err) {
        if (err) {
        // reject(err);
        log.error('Error While Logging ');
        }
        else {
        
        const queryText = `CREATE TABLE IF NOT EXISTS loggerrec( id SERIAL PRIMARY KEY , message TEXT NOT NULL , level TEXT NOT NULL )`;
        client.query(queryText, function (err, result) {
        if (err) {
        console.log("loggerrec");
        log.error(err);
        }
        else {
        client.query('INSERT INTO loggerrec(message , level) values($1, $2) RETURNING id;',
        [req.msg, 'info'],
        function (errObj, result) {
        if (errObj) {
            log.error(errObj);
        }
        else {
        
        req.loggerId = result.rows[0].id;
        console.log(result.rows[0].id, result.rows[0]);
        client.end();
        }
        
        });
        
        }
        
        })
        
        }
        });
      //  });
        }
        

}


