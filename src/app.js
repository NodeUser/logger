const express = require('express');
const router = require('./router/route');
const config = require('./util/config');
const logger = require('./util/MiddleWare');
var winston = require('winston');
const databaseConn = require('./database/dataBaseConnection');
const app = express();
app.use(router);
//databaseConn.connectToDatabse();
//app.use(require("morgan")("combined", { "stream": logger.stream }));
app.use(require("morgan")('combined', { "stream": winston.stream.write}));
app.listen(config.PORT, () => 
console.log(`App listening on port ${config.PORT}`))


